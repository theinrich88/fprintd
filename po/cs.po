# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Marek Černocký <marek@manet.cz>, 2013
# Pavel Borecki <pavel.borecki@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: fprintd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-30 14:00+0200\n"
"PO-Revision-Date: 2021-09-04 23:04+0000\n"
"Last-Translator: Pavel Borecki <pavel.borecki@gmail.com>\n"
"Language-Team: Czech <https://translate.fedoraproject.org/projects/fprintd/"
"fprintd/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n "
"<= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"
"X-Generator: Weblate 4.8\n"

#: data/net.reactivated.fprint.device.policy.in:13
msgid "Verify a fingerprint"
msgstr "Ověřit otisk prstů"

#: data/net.reactivated.fprint.device.policy.in:14
msgid "Privileges are required to verify fingerprints."
msgstr "K ověření otisků prstů jsou zapotřebí oprávnění."

#: data/net.reactivated.fprint.device.policy.in:23
msgid "Enroll new fingerprints"
msgstr "Registrovat nové otisky prstů"

#: data/net.reactivated.fprint.device.policy.in:24
msgid "Privileges are required to enroll new fingerprints."
msgstr "K registraci nových otisků prstů jsou zapotřebí oprávnění."

#: data/net.reactivated.fprint.device.policy.in:33
msgid "Select a user to enroll"
msgstr "Zvolte uživatele k registraci"

#: data/net.reactivated.fprint.device.policy.in:34
msgid "Privileges are required to enroll new fingerprints for other users."
msgstr ""
"K registraci nových otisků prstů pro jiné uživatele jsou zapotřebí oprávnění."

#: src/device.c:690
#, c-format
msgid "Device was not claimed before use"
msgstr "Zařízení nebylo před použitím získáno"

#: src/device.c:699
#, c-format
msgid "Device already in use by another user"
msgstr "Zařízení je již používáno jiným uživatelem"

#: pam/fingerprint-strings.h:50
msgid "Place your finger on the fingerprint reader"
msgstr "Přiložte svůj prst na snímač otisku prstů"

#: pam/fingerprint-strings.h:51
#, c-format
msgid "Place your finger on %s"
msgstr "Přiložte svůj prst na %s"

#: pam/fingerprint-strings.h:52
msgid "Swipe your finger across the fingerprint reader"
msgstr "Přejeďte svým prstem přes snímač otisku prstů"

#: pam/fingerprint-strings.h:53
#, c-format
msgid "Swipe your finger across %s"
msgstr "Přejeďte svým prstem přes %s"

#: pam/fingerprint-strings.h:55
msgid "Place your left thumb on the fingerprint reader"
msgstr "Přiložte palec levé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:56
#, c-format
msgid "Place your left thumb on %s"
msgstr "Přiložte palec levé ruky na %s"

#: pam/fingerprint-strings.h:57
msgid "Swipe your left thumb across the fingerprint reader"
msgstr "Přejeďte palcem levé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:58
#, c-format
msgid "Swipe your left thumb across %s"
msgstr "Přejeďte palcem levé ruky přes %s"

#: pam/fingerprint-strings.h:60
msgid "Place your left index finger on the fingerprint reader"
msgstr "Přiložte ukazováček levé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:61
#, c-format
msgid "Place your left index finger on %s"
msgstr "Přiložte ukazováček levé ruky na %s"

#: pam/fingerprint-strings.h:62
msgid "Swipe your left index finger across the fingerprint reader"
msgstr "Přejďte ukazováčkem levé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:63
#, c-format
msgid "Swipe your left index finger across %s"
msgstr "Přejďte ukazováčkem levé ruky přes %s"

#: pam/fingerprint-strings.h:65
msgid "Place your left middle finger on the fingerprint reader"
msgstr "Přiložte prostředníček levé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:66
#, c-format
msgid "Place your left middle finger on %s"
msgstr "Přiložte prostředníček levé ruky na %s"

#: pam/fingerprint-strings.h:67
msgid "Swipe your left middle finger across the fingerprint reader"
msgstr "Přejeďte prostředníčkem levé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:68
#, c-format
msgid "Swipe your left middle finger across %s"
msgstr "Přejeďte prostředníčkem levé ruky přes %s"

#: pam/fingerprint-strings.h:70
msgid "Place your left ring finger on the fingerprint reader"
msgstr "Přiložte prsteníček levé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:71
#, c-format
msgid "Place your left ring finger on %s"
msgstr "Přiložte prsteníček levé ruky na %s"

#: pam/fingerprint-strings.h:72
msgid "Swipe your left ring finger across the fingerprint reader"
msgstr "Přejeďte prsteníčkem levé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:73
#, c-format
msgid "Swipe your left ring finger across %s"
msgstr "Přejeďte prsteníčkem levé ruky přes %s"

#: pam/fingerprint-strings.h:75
msgid "Place your left little finger on the fingerprint reader"
msgstr "Přiložte malíček levé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:76
#, c-format
msgid "Place your left little finger on %s"
msgstr "Přiložte malíček levé ruky na %s"

#: pam/fingerprint-strings.h:77
msgid "Swipe your left little finger across the fingerprint reader"
msgstr "Přejeďte malíčkem levé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:78
#, c-format
msgid "Swipe your left little finger across %s"
msgstr "Přejeďte malíčkem levé ruky přes %s"

#: pam/fingerprint-strings.h:80
msgid "Place your right thumb on the fingerprint reader"
msgstr "Přiložte palec pravé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:81
#, c-format
msgid "Place your right thumb on %s"
msgstr "Přiložte palec pravé ruky na %s"

#: pam/fingerprint-strings.h:82
msgid "Swipe your right thumb across the fingerprint reader"
msgstr "Přejeďte palcem pravé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:83
#, c-format
msgid "Swipe your right thumb across %s"
msgstr "Přejeďte palcem pravé ruky přes %s"

#: pam/fingerprint-strings.h:85
msgid "Place your right index finger on the fingerprint reader"
msgstr "Přiložte ukazováček pravé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:86
#, c-format
msgid "Place your right index finger on %s"
msgstr "Přiložte ukazováček pravé ruky na %s"

#: pam/fingerprint-strings.h:87
msgid "Swipe your right index finger across the fingerprint reader"
msgstr "Přejeďte ukazováčkem pravé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:88
#, c-format
msgid "Swipe your right index finger across %s"
msgstr "Přejeďte ukazováčkem pravé ruky přes %s"

#: pam/fingerprint-strings.h:90
msgid "Place your right middle finger on the fingerprint reader"
msgstr "Přiložte prostředníček pravé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:91
#, c-format
msgid "Place your right middle finger on %s"
msgstr "Přiložte prostředníček pravé ruky na %s"

#: pam/fingerprint-strings.h:92
msgid "Swipe your right middle finger across the fingerprint reader"
msgstr "Přejeďte prostředníčkem pravé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:93
#, c-format
msgid "Swipe your right middle finger across %s"
msgstr "Přejeďte prostředníčkem pravé ruky přes %s"

#: pam/fingerprint-strings.h:95
msgid "Place your right ring finger on the fingerprint reader"
msgstr "Přiložte prsteníček pravé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:96
#, c-format
msgid "Place your right ring finger on %s"
msgstr "Přiložte prsteníček pravé ruky na %s"

#: pam/fingerprint-strings.h:97
msgid "Swipe your right ring finger across the fingerprint reader"
msgstr "Přejeďte prsteníčkem pravé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:98
#, c-format
msgid "Swipe your right ring finger across %s"
msgstr "Přejeďte prsteníčkem pravé ruky přes %s"

#: pam/fingerprint-strings.h:100
msgid "Place your right little finger on the fingerprint reader"
msgstr "Přiložte malíček pravé ruky na snímač otisku prstů"

#: pam/fingerprint-strings.h:101
#, c-format
msgid "Place your right little finger on %s"
msgstr "Přiložte malíček pravé ruky na %s"

#: pam/fingerprint-strings.h:102
msgid "Swipe your right little finger across the fingerprint reader"
msgstr "Přejeďte malíčkem pravé ruky přes snímač otisku prstů"

#: pam/fingerprint-strings.h:103
#, c-format
msgid "Swipe your right little finger across %s"
msgstr "Přejeďte malíčkem pravé ruky přes %s"

#: pam/fingerprint-strings.h:171 pam/fingerprint-strings.h:199
msgid "Place your finger on the reader again"
msgstr "Přiložte prst na čtečku znovu"

#: pam/fingerprint-strings.h:173 pam/fingerprint-strings.h:201
msgid "Swipe your finger again"
msgstr "Přejeďte prstem znovu"

#: pam/fingerprint-strings.h:176 pam/fingerprint-strings.h:204
msgid "Swipe was too short, try again"
msgstr "Přejetí bylo příliš rychlé, zkuste to znovu"

#: pam/fingerprint-strings.h:178 pam/fingerprint-strings.h:206
msgid "Your finger was not centered, try swiping your finger again"
msgstr "Prst nebyl přiložen na střed, zkuste prstem přejet znovu"

#: pam/fingerprint-strings.h:180 pam/fingerprint-strings.h:208
msgid "Remove your finger, and try swiping your finger again"
msgstr "Sejměte prst a zkuste s ním přejet znovu"

#: pam/pam_fprintd.c:537
msgid "Verification timed out"
msgstr "Překročen časový limit pro ověření"

#: pam/pam_fprintd.c:560
msgid "Failed to match fingerprint"
msgstr "Nepodařilo se najít shodu s otiskem"

#: pam/pam_fprintd.c:576
msgid "An unknown error occurred"
msgstr "Došlo k neznámé chybě"
